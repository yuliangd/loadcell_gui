# loadcell_gui

A Graphical Display for Phidgets loadcell with USB Bridge

## Interface Screenshot
![Picture](concept_interface.png)

## Applicable with
- Button Load Cell (0-200kg) - CZL204E
https://www.phidgets.com/?tier=3&catid=9&pcid=7&prodid=228
![Load Cell](https://www.phidgets.com/productfiles/3137/3137_0/Images/827x-/20/3137_0.jpg)

- PhidgetBridge 4-Input
https://www.phidgets.com/?tier=3&catid=98&pcid=78&prodid=1027
![PhidgetBridge](https://www.phidgets.com/productfiles/1046/1046_0B/Images/827x-/20/1046_0B.jpg)

## How to use?
1. Download and install the Phidget22 library
    https://www.phidgets.com/docs/Language_-_Python

  Scroll down to find 'Windows Drivers Installer (64-Bit)' or 'Linux Libraries' to suit your running environment.
   

2. Clone this to a directory.
    
    `git clone https://gitlab.com/yuliangd/loadcell_gui.git`


3. Create a virtual environment and install required libraries with the script
    
    `bash init.sh` if using Linux
    
    `init.bat` if using Windows PC
    
4. Edit the `config.json` to include the Serial No. of your actual device.


5. Run the code

    `bash run.sh` if using Linux
    
    `run.bat` if using Windows PC

## Authors
- [Yuliang Deng, Keirton Inc](https://gitlab.com/yuliangd)
