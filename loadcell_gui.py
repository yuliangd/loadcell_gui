import json
import os
import random
import tkinter as tk
import tkinter.font as tkFont
from datetime import datetime

import paho.mqtt.client as paho
from Phidget22.Devices.VoltageRatioInput import *
from dotenv import load_dotenv

load_dotenv()

MQTT_BROKER = os.environ.get("MQTT_BROKER")
MQTT_PORT = 1883
MQTT_USER = "twisteriot"
MQTT_PASS = os.environ.get("MQTT_PASS")

mqtt_client = paho.Client("load_cell_log")
mqtt_client.username_pw_set(MQTT_USER, password=MQTT_PASS)
mqtt_client.connect(MQTT_BROKER, MQTT_PORT)  # establish connection

LOG_FILE = datetime.now().strftime("%Y-%m-%d_%H-%M")+".csv"

# GUI generated using https://www.visualtk.com/
DISP_INTERVAL_MS = 250
MQTT_UPLOAD_MS = 30000
COUNTDOWN = int(MQTT_UPLOAD_MS / DISP_INTERVAL_MS)
glb_countdown = max(0, COUNTDOWN)

with open('config.json', "r") as json_file:
    config = json.load(json_file)

SERIAL_NO = config.get("serialno")
KG_RANGE = config.get("max_load")


class App:
    def __init__(self, root):
        # setting title
        root.title("Loadcell Reading")
        # setting window size
        FONT_FAMILY = 'Tahoma'
        COLOR_VAR_BG = '#ffffff'
        width = 360
        height = 300
        screenwidth = root.winfo_screenwidth()
        screenheight = root.winfo_screenheight()
        alignstr = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
        root.geometry(alignstr)
        root.resizable(width=False, height=False)

        self.tare_offset = 0.0
        self.kg_raw = 0.0
        self.kg_mem = 0.0
        self.kg_display_obj = tk.StringVar()
        self.kg_mem_obj = tk.StringVar()
        self.updateKg_disp()

        button_tare = tk.Button(root)
        button_tare["bg"] = "#efefef"
        button_tare["font"] = tkFont.Font(family=FONT_FAMILY, size=14)
        button_tare["fg"] = "#000000"
        button_tare["justify"] = "center"
        button_tare["text"] = "TARE\n(ZERO)"
        button_tare.place(x=20, y=190, width=140, height=54)
        button_tare["command"] = self.tare_command

        button_keep = tk.Button(root)
        button_keep["bg"] = "#efefef"
        button_keep["font"] = tkFont.Font(family=FONT_FAMILY, size=14)
        button_keep["fg"] = "#000000"
        button_keep["justify"] = "center"
        button_keep["text"] = "KEEP"
        button_keep.place(x=190, y=190, width=140, height=54)
        button_keep["command"] = self.mem_command

        label_value_mem = tk.Label(root, textvariable=self.kg_mem_obj)
        label_value_mem["font"] = tkFont.Font(family=FONT_FAMILY, size=12)
        label_value_mem["fg"] = "#333333"
        label_value_mem["bg"] = COLOR_VAR_BG
        label_value_mem["justify"] = "left"
        label_value_mem.place(x=272, y=152, width=62, height=30)

        label_kg = tk.Label(root)
        label_kg["font"] = tkFont.Font(family=FONT_FAMILY, size=36)
        label_kg["fg"] = "#333333"
        label_kg["justify"] = "center"
        label_kg["text"] = "KG"
        label_kg.place(x=260, y=52, width=70, height=48)

        label_value_kg = tk.Label(root, textvariable=self.kg_display_obj)
        label_value_kg["bg"] = COLOR_VAR_BG
        label_value_kg["font"] = tkFont.Font(family=FONT_FAMILY, size=72)
        label_value_kg["fg"] = "#00ced1"
        label_value_kg["justify"] = "right"
        label_value_kg["relief"] = "groove"
        label_value_kg.place(x=20, y=40, width=240, height=120)

        label_sn = tk.Label(root)
        ft = tkFont.Font(family=FONT_FAMILY, size=10)  # common font setting for the belows
        label_sn["font"] = ft
        label_sn["fg"] = "#333333"
        label_sn["justify"] = "right"
        label_sn["text"] = "Device Serial No"
        label_sn.place(x=10, y=260, width=112, height=30)

        label_value_sn = tk.Label(root)
        label_value_sn["font"] = ft
        label_value_sn["fg"] = "#333333"
        label_value_sn["bg"] = COLOR_VAR_BG
        label_value_sn["justify"] = "left"
        label_value_sn["text"] = SERIAL_NO
        label_value_sn.place(x=116, y=260, width=98, height=30)

        label_range = tk.Label(root)
        label_range["font"] = ft
        label_range["fg"] = "#333333"
        label_range["justify"] = "right"
        label_range["text"] = "Range"
        label_range.place(x=204, y=260, width=62, height=30)

        label_value_range = tk.Label(root)
        label_value_range["font"] = ft
        label_value_range["fg"] = "#333333"
        label_value_range["bg"] = COLOR_VAR_BG
        label_value_range["justify"] = "left"
        label_value_range["text"] = "0 - " + str(KG_RANGE) + " kg"
        label_value_range.place(x=264, y=260, width=80, height=31)

    def tare_command(self):
        self.tare_offset = self.kg_raw
        self.updateKg_disp()
        print('Zeroed')

    def mem_command(self):
        self.mem_kg = self.kg_raw - self.tare_offset
        kg_str = "{:.1f}".format(self.mem_kg)
        self.kg_mem_obj.set(kg_str)
        print("{}, {} kg".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"), kg_str))

    def updateKg_disp(self):
        kg_float = self.kg_raw - self.tare_offset
        kg_str = "{:.1f}".format(kg_float)
        self.kg_display_obj.set(kg_str)
        # print(kg_str)
        return kg_float

    def setKG_raw(self, kg):
        self.kg_raw = kg
        return self.updateKg_disp()


def volratio_to_kg(voltageRatio):
    return voltageRatio * KG_RANGE * 1000


def update_to_gui(vr):
    kg_raw = volratio_to_kg(vr)
    # print('kg raw = {}'.format(kg_raw))
    kg_calib = app.setKG_raw(kg_raw)
    global glb_countdown
    glb_countdown = glb_countdown - 1
    if glb_countdown < 0:
        timestr = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        ret = mqtt_client.publish("dt/loadcell/rollertester", json.dumps({"kg": round(kg_calib, 1)}))
        if ret == 0:
            print("{} kg".format(kg_calib))
        with open(os.path.join("data", LOG_FILE), 'w+') as f:
            f.write(timestr + ", " + str(round(kg_calib, 2))+"\n")
        glb_countdown = COUNTDOWN


def fake_sensor():
    update_to_gui(random.random() / 50 + 0.02)
    root.after(DISP_INTERVAL_MS, fake_sensor)


def read_sensor():
    vr = voltageRatioInput0.getVoltageRatio()
    update_to_gui(vr)
    root.after(DISP_INTERVAL_MS, read_sensor)


# def onVoltageRatioChange(self, voltageRatio):
#   update_to_gui(voltageRatio)

root = tk.Tk()
voltageRatioInput0 = VoltageRatioInput()
voltageRatioInput0.setDeviceSerialNumber(SERIAL_NO)
app = App(root)
# voltageRatioInput0.setOnVoltageRatioChangeHandler(onVoltageRatioChange)
root.after(DISP_INTERVAL_MS, read_sensor)
voltageRatioInput0.openWaitForAttachment(5000)
root.mainloop()
voltageRatioInput0.close()
